#!/usr/bin/env python3
# turtle: hw.py
import turtle
print("hello world = hw")
t = turtle.Turtle()# t = turtle object
s = turtle.Screen()# s = screen object
t.showturtle()
t.pendown()# t.pd()
t.pencolor("red")
t.pensize(7)
t.lt(90)
t.fd(60)
t.bk(30)
t.rt(90)
t.fd(30)
t.lt(90)
t.fd(30)
t.bk(60)
t.pu()
t.rt(90)
t.fd(30)
t.lt(90)
t.fd(60)
t.lt(30)
t.pencolor("blue")
t.pd()
t.bk(70)
t.rt(60)
t.fd(40)
t.rt(120)
t.pencolor("#ff7f00")
t.fd(40)
t.lt(120)
t.fd(70)
t.ht()#hide turtle
s.exitonclick()	

# https://icebowl.io/python/
# https://gitlab.com/icebowl/youtube/-/tree/main/turtle
#https://realpython.com/beginners-guide-python-turtle/
# https://docs.python.org/3/library/turtle.html
