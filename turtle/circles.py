#!/usr/bin/env python3
# turtle: hw.py
import turtle
print("circles")
t = turtle.Turtle()# t = turtle object
s = turtle.Screen()# s = screen object
t.pencolor("#00ff00")
t.pensize(5)
t.goto(0,0) # x = 0 y = 0
t.circle(50)
t.goto(100,100)# x = 100 y = 100
t.circle(50)
t.goto(-100,100) # x = -100 y = 100
t.circle(50)
t.goto(-100,-100) # x = -100 y = -100
t.circle(50)
t.goto(100,-100) # x = 100 y = -100
t.circle(50)
# draw  xy
t.pu()
t.pensize(1)
t.pencolor("#0000ff")
t.goto(-300,0)
t.pd()
t.goto(300,0)
t.pu()
t.goto(0,300)
t.pd()
t.goto(0,-300)



s.exitonclick()	

# https://icebowl.io/python/
# https://gitlab.com/icebowl/youtube/-/tree/main/turtle
#https://realpython.com/beginners-guide-python-turtle/
# https://docs.python.org/3/library/turtle.html
