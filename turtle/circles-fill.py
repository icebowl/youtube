#!/usr/bin/env python3
# turtle: hw.py
import turtle
print("hello world = hw")
t = turtle.Turtle()# t = turtle object
s = turtle.Screen()# s = screen object

t.fillcolor("#ffff00") # set the fillcolor
  
# start the filling color
t.begin_fill()

t.circle(50)  
# ending the filling of the color
t.end_fill()

t.fillcolor("#0000ff")
t.begin_fill()
  
t.circle(50)
  
# ending the filling of the color
t.end_fill()



s.exitonclick()	

# https://icebowl.io/python/
# https://gitlab.com/icebowl/youtube/-/tree/main/turtle
#https://realpython.com/beginners-guide-python-turtle/
# https://docs.python.org/3/library/turtle.html
