import subprocess

word_list = []
with open('10000words.txt') as my_file:
    for line in my_file:
        word_list.append(line)

#printing the array
#for word in word_list:
#    print(word, end = ' ')

for word in word_list:
	result = "grep -o '"+word+"' 1000000000chars.txt | wc -l"
	command = subprocess.check_output(result, shell=True)
#	print("result - ",word," ",command," ",end='')
	print(word," ",end='')
	print(command.decode('utf-8'))

'''
UTF-8 - an ASCII compatible multibyte Unicode encoding

UTF-8 stands for “UCS Transformation Format 8,” 
and it is the most common form of character encoding for Unicode. 
Here, each unique character is mapped to one-byte units.
 And since one byte consists of 8 bits, thus, UTF includes “-8” in its name.
  These bytes are finally interpreted by the language processor into binary
   for the processor to understand our human language. 
'''
