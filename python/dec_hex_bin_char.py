#!/usr/bin/env python3
def dec_to_bin(x):
	binString = str(int(bin(x)[2:]))
	addZeros = 8 - len(binString)
	for i in range (0,addZeros):
		binString = "0" + binString
	#print("slength ",addZeros)
	binString = binString[:4] + ' ' + binString[4:]
	return binString

def dec_to_hex(x):
	hexString = str(hex(x)[2:])
	if len(hexString) < 2:
		hexString = "0"+hexString
	return hexString

def main():
	print ("\tDEC BIN HEX CHARACTER")
	for n in range (0,256):
		binaryString = dec_to_bin(n)
		hexadecimalString = dec_to_hex(n)
		character = chr(n)
		print ("\t"+str(n)+" "+binaryString+" "+hexadecimalString+" "+character)
		
if __name__ == '__main__': 
	main()
#code by cwcoleman 230616 - have fun	
'''   
https://realpython.com/if-name-main-python/
'''
