#!/usr/bin/env python3
import turtle
s = turtle.getscreen()
t = turtle.Turtle()
t.right(90)
t.forward(100)
t.left(90)
t.backward(100)
t.exitonclick()
