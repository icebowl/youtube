#!/usr/bin/env python3
def dec_to_bin(x):
    return int(bin(x)[2:])

def dec2bin(x): #only works with decimals less than 256
	binString = ""
	remainder = 0
	dividend = x
	for n in range (0,8):
		remainder = dividend % 2
		dividend = dividend  // 2 #integer div
		#print(remainder," ",end="") # debug
		binString = binString + str(remainder) #concatinate with type cast of str
	return binString

def reverseString(ns):
	n = len(ns)
	rs = "" # reverse string
	for i in range (n-1,-1,-1):
		rs = rs + ns[i]
		if i == 4:
			rs = rs +" " #show the nibble (half of a byte)

	#print (ns,rs)
	return rs

def main():
	for n in range (0,256):
		#print ("\n * ",n,"\n")
		#print(n, dec_to_bin(n))
		binaryString = dec2bin(n)
		#print ("\n\n"+binaryString)
		binaryString = reverseString(binaryString)
		print ("\t"+str(n)+" base 10 = "+binaryString+" "+str(chr(n)))

if __name__ == '__main__':
	main()

#code by cwcoleman - have fun 
'''
https://realpython.com/if-name-main-python/
'''
