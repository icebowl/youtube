// hello.c (file name)
// compile: gcc hello.c -o hello.o
// run ./hello.o

#include <stdio.h>

int main(){
	printf("Hello, World!\n\n");
	printf("ABCDEF\n\n");
	printf("**********************\n");
	printf("*  ccc  w     w  ccc *\n");
	printf("* c      w w w  c    *\n");
	printf("*  ccc    w w    ccc *\n");
	printf("**********************\n");
	return 0;
}//end main() ENCAPSULATION
// c is a function based language

/*
	-
	-
	-

*/
