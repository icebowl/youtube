//cvars2.c
#include <stdio.h>

int main() {
	// 1 111 1111 1111 1111 
	unsigned short int usi1,usi2,sumsu;
	short int  si1,si2,sumsi;
	
	unsigned int ui;
	int h,i,j,k,sumi;
	//short integer math
	usi1 = 127; usi2 = 128; 
	sumsu = (usi2 - usi1);
	printf("sumsu %u \n\n",sumsu);
	
	si1 = 65534; si2 = 65535;
	sumsi =  si1 - si2 ;
	printf("sumu %d \n\n",sumsi);
	
	// integer math
	h = 2; i = 3; j = 5; k = 7;
	sumi = h+i+j+k+4294967296429496727;
	printf("sumu %d \n\n",sumi);
	return 0;
}//end main()

/* 1111 1111 1111 1111 base 2 = 65535 base 10
 * 1+2+4+8+16+32+64+128+256+512+1024+2048+4096+8192+16384+32768
 * https://en.wikipedia.org/wiki/C_data_types
 * https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.html#Primitive-Types
 * https://cplusplus.com/reference/cstdio/printf/
 * 
 %c 	character
%d 	decimal (integer) number (base 10)
%e 	exponential floating-point number
%f 	floating-point number
%i 	integer (base 10)
%o 	octal number (base 8)
%s 	a string of characters
%u 	unsigned decimal (integer) number
%x 	number in hexadecimal (base 16)
%% 	print a percent sign
\% 	print a percent sign
    int - stores integers (whole numbers), without decimals, such as 123 or -123
    float - stores floating point numbers, with decimals, such as 19.99 or -19.99
    char - stores single characters, such as 'a' or 'B'. Char values are surrounded by single quotes
*/
