// c variables  Primitive Data Types
// gcc cvars1.c -o cvars.o
#include <stdio.h>

int main(){
	int ii = 65;  // integer  
	float ef = 2.71828; //float
	double pi = 3.1415926535897932384626433832795028841971;// double float
	double product;
	char a = 'A';
	printf("char %c = %i-  \n",a,ii);	
	printf("integer %i - \n", ii);
	printf("float %f - \n", ef);
	printf("double pi = %.20f - \n",pi);
	printf("double 4pi = %.43f -  \n",2*pi);
	product = (double)ii*(double)ef*pi;
	printf("product = %f \n\n",product);
  return 0;
}//end main()

/*
 * https://en.wikipedia.org/wiki/C_data_types
 * https://www.gnu.org/software/gnu-c-manual/gnu-c-manual.html#Primitive-Types
 * https://cplusplus.com/reference/cstdio/printf/
 * https://www.w3schools.com/c/c_variables.php
    int - stores integers (whole numbers), without decimals, such as 123 or -123
    float - stores floating point numbers, with decimals, such as 19.99 or -19.99
    char - stores single characters, such as 'a' or 'B'. Char values are surrounded by single quotes
*/
