#include <iostream>
//#include <array>
using namespace std; // c++ namespace

int decimalFromHex(string h); //initialize function decimal from hex string
void print2Darray(int values[3][3]); //void function print2Darry
	
int main(int argc, char **argv){
		
	//initialized variables
	string hexs1, hexs2, hexs3;
	int intin,red,green,blue;
	float floatin;
	int rgb_values[3][3]; //2d array 3x3
	
	// output command line arguments (inputs)
	cout<< " argv[0] "<<argv[0]<<endl;
    cout<<" hex string 1 "<<argv[1]<<endl;
    cout<<" hex string 2 "<< argv[2]<<endl;
    cout<<" hex string 3 "<<argv[3]<<endl;
    cout<<" integer "<< argv[4]<<endl;
    cout<<" float "<< argv[5]<<endl;
    
	//assign command line arguments to variables
	hexs1 = argv[1]; hexs2 = argv[2]; hexs3 = argv[3];
    intin = stoi(argv[4]); floatin = stof(argv[5]);
    
	// call function to convert from hexadecimal to decimal
	//                sub string 012345			
	red = decimalFromHex(hexs1.substr(0,2));
	green = decimalFromHex(hexs1.substr(2,2));
	blue = decimalFromHex(hexs1.substr(4,2));
	cout<<"rgb\t"<<hexs1<<"\t"<<red<<"\t"<<green<<"\t"<<blue<<endl;
	//aasign variables to array
	rgb_values[0][0]= red;
	rgb_values[0][1]= green;
	rgb_values[0][2]= blue;
	red = decimalFromHex(hexs2.substr(0,2));
	green = decimalFromHex(hexs2.substr(2,2));
	blue = decimalFromHex(hexs2.substr(4,2));
	cout<<"rgb\t"<<hexs2<<"\t"<<red<<"\t"<<green<<"\t"<<blue<<endl;
	rgb_values[1][0]= red;
	rgb_values[1][1]= green;
	rgb_values[1][2]= blue;
	red = decimalFromHex(hexs3.substr(0,2));
	green = decimalFromHex(hexs3.substr(2,2));
	blue = decimalFromHex(hexs3.substr(4,2));
	cout<<"rgb\t"<<hexs3<<"\t"<<red<<"\t"<<green<<"\t"<<blue<<endl;
	rgb_values[2][0]= red;
	rgb_values[2][1]= green;
	rgb_values[2][2]= blue;
	print2Darray(rgb_values);// print 2d array
    return 0;
}

// two digit hex to decimal conversion
int decimalFromHex(string h) {
	int d10,d1; //
	char sixteens,ones;
	//cout<<" h "<<h<<endl;
	sixteens = h[0]; //sixteenths place
	ones = h[1];     // ones place
	d10 = int(sixteens);
	if (d10 < 97){
		d10 = (d10 - 48)*16;
	}else{
		d10 = (d10 - 87)*16;
	}
	d1 = int(ones);
	if (d1 < 97){
		d1 = (d1 - 48);
	}else{
		d1 = (d1 - 87);
	}
	// debug cout
	//char c = 'g';
	//cout<<" 16 1 * "<<sixteens<<","<<ones<<endl;
	//cout<<" 10 1 "<<d10<<","<<d1<<endl;
	//cout<<" d10+d1 "<<d10+d1<<endl;
	return d10+d1;
}

void print2Darray(int values[3][3] ){
	int r,c;
	cout<<"print 3x3 array"<<endl;
	for (r = 0; r < 3; r++){
		for (c = 0; c < 3; c++){
			cout<<values[r][c]<<" ";
		}	
		cout<<"\n";
	}
}
